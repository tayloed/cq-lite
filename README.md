# README #

This is the python script for an algo implemented on CloudQuant.com. 

It will only run on CloudQuant.


### How do I get set up? ###

* Create an account on CloudQuant.com
* Create a script in your CloudQuant scripts directory
* Copy this algo into your script (copy & paste)

### Contribution guidelines ###

* Cite any refrences to external sources.
* Explain WHY you did what you did, not just what you did.
