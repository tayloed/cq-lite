from cloudquant.interfaces import Strategy


###############################################################################################
# Bearish TD Sequential Price Flip to Initiate a buy of a stock.
#
# This strategy is documented in Jason Perl's book DeMark Indicators on page 3.
# ISBN 978-1-57660-314-7 Bloomberg Press, New York 2008
# http://amzn.to/2pm0Uv9
#
# The basic concept of this strategy is to determine when a down market is likely to become
# a flip into an up market. This could be an intraday strategy. That is how I have implemented
# it in this code.
#
# Components of this strategy:
# TD Bearish Price Flip.
# This price flip is a pre-requisite to the strategy. We must have this condition prior to
# receiving the buy indication. The bear flip occurs when the close of a bar is greater than
# the close 4 bars earlier immediately followed by a close less than 4 bars earlier.
#
# The Countdown:
# Once a Bear Flip has occured then we check for an uninterrupted series of 9 closes with
# each close being less than the close 4 bars earlier.
#
# Once we have a flip, and then a uninterrupted series of 9 then we buy at the market.
#
# Taking Profit or Loss:
# We watch the bar data to ensure that we take a profit or loss while trading.
#
# A few other thoughts on why I wrote this algo.
# 1. I wanted to learn the CloudQuant light programming interfaces. To see what worked for me
# 2. Long only strategy. This avoids any complications that could come from short selling.
# 3. I wanted to take gains quickly.
# 4. I want to be willing to take losses, but only to a certain amount.
# 5. I limited the strategy to a few stocks for my backtesting.
#
# Ideas for improving this algo
# 1. Do a data study to optimize the target price for loss and profit taking.
# 2. Do a data study to see if there is a max period of time that you should hold a position.
# 3. Utilize other data other than just the close price in each bar
#    3.1 Does the BarView open, high, low affect the success rate?
#    3.2 Does the bidvol, or askvol affect the success rate?
#    3.3 Does the vwap affect success rate?
#    3.4 Does the spread between bid price and ask price change with success rates?
# 4. Read the DeMark indicator book and apply more of the techniques past page 3 :)
# 5. Change the bar length to see if you can do this as a different time length
# 6. Use a data driven approach to pick which stocks to run this algo upon
# 7. Are there times of day approaches that would work better? Do the study.
# 8. Does this strategy work for a market that is trending up or down better?
# 9. Does this stragegy work better for market capitalization size?
# 10. This is implemented as a buy (long only) strategy. Implementing the sell short strategy
#    could improve the returns.
#
##############################################################################################

# class CQ0699056ec7c8407ebe3cfa158b034532(Strategy):  ## orignal name that was generated, i changed it!
class CQ_TD_Sequential_Base(Strategy):
    ########################################################################################
    #
    # high level strategy - start/finish
    #
    ########################################################################################

    # called when the strategy starts (aka before anything else)
    @classmethod
    def on_strategy_start(cls, md, service, account):
        pass

    # called when the strategy finish (aka after everything else has stopped)
    @classmethod
    def on_strategy_finish(cls, md, service, account):
        pass

        ########################################################################################

    #
    # symbol universe
    #
    ########################################################################################

    # note that this doesn't start with "self" because it's a @classmethod
    @classmethod
    def is_symbol_qualified(cls, symbol, md, service, account):
        # Return only the symbols that I am interested in trading or researching.
        return symbol in ['EBAY', 'HD', 'MSFT', 'SIRI', 'UNP', 'UPS', 'WBA']

    # used to load other symbols data not in is_symbol_qualified(). Only used in backtesting
    @classmethod
    def backtesting_extra_symbols(cls, symbol, md, service, account):
        return False

    ########################################################################################
    #
    # start/finish instance related methods
    #
    ########################################################################################

    # used to pass external parameters for each instance (same for values for every instance)
    def __init__(self):  # , **params - if passing in parameters is desired

        self.count = 0  # count of times that the TD Bear Flip occcurs
        self.TD_BearFlipFlag = False  # flag indicating that a bear flip has occurred
        self.TD_countdownInProgress = False  # Flag to indicate that we are looking for the countdown after a bear flip.
        self.TD_countdown = 0;
        self.IsPositionOn = False  # do we have a position on?
        self.LongQty = 0  # long quantity of our position
        self.long_entry_price = 0  # estimated price of our position
        self.FileName = "CQ_TD_Sequential_Base.txt"

    # called at the beginning of each instance
    def on_start(self, md, order, service, account):
        print "OnStart {0}\t{1}\n".format(service.time_to_string(service.system_time), self.symbol)
        # variable to track TD Setup condition.
        self.TD_BearFlipFlag = False

        # The model requires that we have at least 4 minutes of bar data prior
        # to checking to see if a bear price flip has occurred. Therefore we
        # need a variable to track this start time.
        self.model_start = md.market_open_time + service.time_interval(minutes=5)

    # if running with an instance per symbol, call when an instance is terminated
    def on_finish(self, md, order, service, account):
        print "OnFinish {0}\t{1} bear flip count{2}\n".format(service.time_to_string(service.system_time), self.symbol,
                                                              self.count)

    ########################################################################################
    #
    # timer method
    #
    ########################################################################################

    # called in timer event is received
    def on_timer(self, event, md, order, service, account):
        pass

    ########################################################################################
    #
    # market data related methods
    #
    ########################################################################################

    # called every minute before the first on_trade of every new minute, or 5 seconds after a new minute starts
    def on_minute_bar(self, event, md, order, service, account, bar):
        #
        # don't want to initiate any long positions in the last 15 minutes of the trading day
        # as we won't likely have time to trade out of the position for a profit using 1 minute
        # bar data.
        #
        if service.system_time < md.market_close_time - service.time_interval(minutes=16, seconds=1):
            #
            # If a position is on we want to check to see if we should take a profit or trade out
            # of a losing position.
            #
            if self.IsPositionOn == True:
                # there is a position on, therefore we want to check to see if
                # we should realize a profit or stop a loss
                bar_0 = bar.minute()
                if len(bar_0) > 0:
                    bv_0 = bar_0.low
                    if bv_0[0] > self.long_entry_price + .15:
                        # target profit realized, we want to get out of the position.
                        print "\texit position now {0}\t{1} entry px at {2} low of bar = {3}\n".format(
                            service.time_to_string(service.system_time), self.symbol, self.long_entry_price, bv_0[0])

                        self.IsPositionOn = False
                        # send order; use a variable to accept the order_id that order.algo_buy returns
                        sell_order_id = order.algo_sell(self.symbol, "market", intent="exit")

                    elif bv_0[0] < self.long_entry_price - .30:
                        # we are losing more than twice our target profit, we therefore
                        # want to stop our losses and trade out of the position.
                        print "\texit losing position now {0}\t{1} entry px at {2} low of bar = {3}\n".format(
                            service.time_to_string(service.system_time), self.symbol, self.long_entry_price, bv_0[0])
                        self.IsPositionOn = False
                        # send order; use a variable to accept the order_id that order.algo_buy returns
                        sell_order_id = order.algo_sell(self.symbol, "market", intent="exit")

            # we have to have at least 5 minutes of bar data before we can start checking to see if a bear flip has occurred.
            if md.L1.timestamp > self.model_start:
                if self.TD_countdownInProgress == False:
                    bar_1 = bar.minute(start=-5, include_empty=True)
                    bv = bar_1.close
                    # Check to see if we have a TD bear flip
                    if len(bar_1.close) == 5:
                        if bv[0] < bv[3] and bv[0] > 0 and bv[3] > 0:
                            if bv[4] < bv[1] and bv[4] > 0 and bv[4] > 0:
                                self.count += 1
                                self.TD_BearFlipFlag = True
                                self.TD_countdownInProgress = True
                                # print "TD Bear Flip found {0}\t{1}\n".format(service.time_to_string(service.system_time), self.symbol)
                            else:
                                self.TD_BearFlipFlag = False
                else:
                    self.TD_countdown += 1
                    # if self.TD_countdown == 8:
                    if self.TD_countdown >= 4:
                        bar_2 = bar.minute(start=-13, include_empty=True)
                        bv2 = bar_2.close
                        ###############################################################
                        # Check each in the series to see if we have a full countdown
                        # which is the 9 uninteruped bars where the close is less than
                        # the close 4 bars earlier.
                        ###############################################################
                        if len(bar_2.close) == 13:
                            if bv2[0] > bv2[4] \
                                    and bv2[1] > bv2[5] \
                                    and bv2[2] > bv2[6] \
                                    and bv2[3] > bv2[7] \
                                    and bv2[4] > bv2[8] \
                                    and bv2[5] > bv2[9] \
                                    and bv2[6] > bv2[10] \
                                    and bv2[7] > bv2[11] \
                                    and bv2[8] > bv2[12]:
                                if self.IsPositionOn == False:
                                    print "buy now {0}\t{1}\n".format(service.time_to_string(service.system_time),
                                                                      self.symbol)
                                    self.IsPositionOn = True
                                    # send order; use a variable to accept the order_id that order.algo_buy returns
                                    order_id = order.algo_buy(self.symbol, "market", intent="init", order_quantity=100)
                                    print("100 shares of " + self.symbol + " have been ordered! \n\n")

                                    ########################################################################
                                    # since on_trade and on_fill aren't called in lite, use the close bar px
                                    # as our entry px approximate. We will use this price to check for
                                    # profit taking or for stop loss.
                                    self.long_entry_price = bv2[12];

                            self.TD_countdown = 0
                            self.TD_countdownInProgress = False


        else:
            ####################################################################
            # close out of our position at the end of the day because we don't
            # want to carry overnight risk.
            if self.IsPositionOn == True:
                sell_order_id = order.algo_sell(self.symbol, "market", intent="exit")
                self.IsPositionOn = False
                print "\texit EOD position now {0} for {1}\n".format(service.time_to_string(service.system_time),
                                                                     self.symbol)

    # called when time and sales message is received
    # NOT CALLED for CloudQuant LITE
    # on_trade is called when a trade is printed in the market data.
    #
    def on_trade(self, event, md, order, service, account):
        pass

    # called when national best bid offer (nbbo) prices change (not size)
    # NOT CALLED for CloudQuant LITE
    #
    # This would be a great place to check for profit or loss changes
    def on_nbbo_price(self, event, md, order, service, account):
        pass

    # called when arca imbalance message is received
    # NOT CALLED for CloudQuant LITE
    def on_arca_imbalance(self, event, md, order, service, account):
        pass

    # called when nasdaq imbalance message is received
    # NOT CALLED for CloudQuant LITE
    def on_nasdaq_imbalance(self, event, md, order, service, account):
        pass

    # called when nyse/amex/nsye mkt/openbook message is received
    # NOT CALLED for CloudQuant LITE
    def on_nyse_imbalance(self, event, md, order, service, account):
        pass

    ########################################################################################
    #
    # order related methods
    #
    ########################################################################################

    # called when order considered pending by the system
    # NOT CALLED for CloudQuant LITE
    def on_ack(self, event, md, order, service, account):
        pass

    # called when an order is rejected (locally or other parts of the order processes e.g. the market)
    # NOT CALLED for CloudQuant LITE
    def on_reject(self, event, md, order, service, account):
        pass

    # called when the position changes in this account and symbol (whether manually or automated)
    # NOT CALLED for CloudQuant LITE
    def on_fill(self, event, md, order, service, account):
        #
        # ######################################################################
        # save the entry price if going long
        # set the IsPoisitionOn flag to True so that we begin to look for ways
        # to trade out of the position
        #
        # not called in Cloud lite!!!!!!!
        if (event.intent == 'init' or event.intent == 'increase'):
            self.long_entry_price = account[self.symbol].position.entry_price
            self.IsPositionOn = True
            print "Filled Adding Position on {0} px:{1}\tTime:{2}\n".format(self.symbol, self.long_entry_price,
                                                                            service.time_to_string(service.system_time))
        else:
            self.IsPositionOn = False
            print "Filled Closing Position on {0} px:{1}\tTime:{2}\n".format(self.symbol, event.price,
                                                                             service.time_to_string(
                                                                                 service.system_time))

    # called when the market has confirmed an order has been canceled
    # NOT CALLED for CloudQuant LITE
    def on_cancel(self, event, md, order, service, account):
        pass